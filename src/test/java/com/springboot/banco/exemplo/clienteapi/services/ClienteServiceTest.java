package com.springboot.banco.exemplo.clienteapi.services;

import com.springboot.banco.exemplo.clienteapi.entities.Cliente;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest
class ClienteServiceTest {

    @Autowired
    private  ClienteService clienteService;

    @Test
    @Order(1)
    void salvarPrimeiroCliente() throws ParseException {
        Cliente primeiroCliente = new Cliente();
        primeiroCliente.setNome("Rafael Neto");
        primeiroCliente.setEmail("rafael.neto@outlook.com");
        primeiroCliente.setCpf("43236147806");
        primeiroCliente.setDataNascimento(new SimpleDateFormat("dd/MM/yyyy").parse("28/09/1987"));

        primeiroCliente = clienteService.save(primeiroCliente);

        assertEquals(1, primeiroCliente.getId());
    }

    @Test
    @Order(2)
    void salvarSegundoCliente() throws ParseException {
        Cliente segundoCliente = new Cliente();
        segundoCliente.setNome("Andressa Saraiva");
        segundoCliente.setEmail("andressa.saraiva@outlook.com");
        segundoCliente.setCpf("38874213450");
        segundoCliente.setDataNascimento(new SimpleDateFormat("dd/MM/yyyy").parse("26/12/1992"));

        segundoCliente = clienteService.save(segundoCliente);

        assertEquals(2, segundoCliente.getId());
    }

}
