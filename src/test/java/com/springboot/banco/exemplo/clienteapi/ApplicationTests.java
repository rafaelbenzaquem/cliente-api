package com.springboot.banco.exemplo.clienteapi;

import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class ApplicationTests {

	@Value("${local.server.port}")
	protected int port;

	@BeforeEach
	public void setUp() {
		RestAssured.port = port;
	}

}
