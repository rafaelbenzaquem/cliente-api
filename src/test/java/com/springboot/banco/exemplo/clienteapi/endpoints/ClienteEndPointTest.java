package com.springboot.banco.exemplo.clienteapi.endpoints;

import com.springboot.banco.exemplo.clienteapi.ApplicationTests;
import com.springboot.banco.exemplo.clienteapi.entities.Cliente;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
 class ClienteEndPointTest extends ApplicationTests {

    @Test
    @Order(1)
    void cadastrarPrimeiroCliente() throws ParseException {

        Cliente primeiroCliente = new Cliente();
        primeiroCliente.setNome("Rafael Neto");
        primeiroCliente.setEmail("rafael.neto@outlook.com");
        primeiroCliente.setCpf("43236147806");
        primeiroCliente.setDataNascimento(new SimpleDateFormat("dd/MM/yyyy").parse("28/09/1987"));

        given().
                contentType("application/json").
                body(primeiroCliente).
                when().
                post("/clientes").
                then().
                statusCode(201)
                .header("Location", "http://localhost:" + port + "/clientes/1");

    }

    @Test
    @Order(2)
     void cadastrarSegundoCliente() throws ParseException {

        Cliente segundoCliente = new Cliente();
        segundoCliente.setNome("Andressa Saraiva");
        segundoCliente.setEmail("andressa.saraiva@outlook.com");
        segundoCliente.setCpf("38874213450");
        segundoCliente.setDataNascimento(new SimpleDateFormat("dd/MM/yyyy").parse("26/12/1992"));

        given().
                contentType("application/json").
                body(segundoCliente).
                when().
                post("/clientes").
                then().
                statusCode(201)
                .header("Location", "http://localhost:" + port + "/clientes/2");

    }

    @Test
    @Order(3)
     void cadastrarClienteComEmailRepetidoError() throws ParseException {
        Cliente cliente = new Cliente();
        cliente.setNome("Rafael da Silva Neto");
        cliente.setEmail("rafael.neto@outlook.com");
        cliente.setCpf("77643070253");
        cliente.setDataNascimento(new SimpleDateFormat("dd/MM/yyyy").parse("28/09/1987"));

        given().
                contentType(ContentType.JSON).
                body(cliente).
                when().
                post("/clientes").
                then().
                statusCode(400).
                contentType(ContentType.JSON).
                body("message", equalTo("Erro de validação!")).
                body("errors[0].fieldName", equalTo("email")).
                body("errors[0].fieldMessage", equalTo("Email repetido ou nulo!"));

    }

    @Test
    @Order(4)
    void cadastrarClienteComEmailNuloError() throws ParseException {
        Cliente cliente = new Cliente();
        cliente.setNome("Rafael da Silva Neto");
        cliente.setCpf("22431356558");
        cliente.setDataNascimento(new SimpleDateFormat("dd/MM/yyyy").parse("28/09/1987"));

        given().
                contentType(ContentType.JSON).
                body(cliente).
                when().
                post("/clientes").
                then().
                statusCode(400).
                contentType(ContentType.JSON).
                body("message", equalTo("Erro de validação!")).
                body("errors[0].fieldName", equalTo("email")).
                body("errors[0].fieldMessage", equalTo("Campo 'email' não pode ser nulo ou vazio!"));

    }

    @Test
    void cadastrarClienteComEmailInvalidoError() throws ParseException{
        Cliente cliente = new Cliente();
        cliente.setNome("Maria da Silva");
        cliente.setEmail("asdasdasdasd");
        cliente.setCpf("36586588510");
        cliente.setDataNascimento(new SimpleDateFormat("dd/MM/yyyy").parse("28/09/1987"));

        given().
                contentType(ContentType.JSON).
                body(cliente).
                when().
                post("/clientes").
                then().
                statusCode(400).
                contentType(ContentType.JSON).
                body("message", equalTo("Erro de validação!")).
                body("errors[0].fieldName", equalTo("email")).
                body("errors[0].fieldMessage", equalTo("Compo 'email' mal formado."));
    }

    @Test
    void cadastrarClienteComCPFInvalidoError() throws ParseException{
        Cliente cliente = new Cliente();
        cliente.setNome("Maria da Silva");
        cliente.setEmail("maria@gmail.com");
        cliente.setCpf("12345678912");
        cliente.setDataNascimento(new SimpleDateFormat("dd/MM/yyyy").parse("28/09/1987"));

        given().
                contentType(ContentType.JSON).
                body(cliente).
                when().
                post("/clientes").
                then().
                statusCode(400).
                contentType(ContentType.JSON).
                body("message", equalTo("Erro de validação!")).
                body("errors[0].fieldName", equalTo("cpf")).
                body("errors[0].fieldMessage", equalTo("Campo 'cpf' mal formado."));
    }
}
