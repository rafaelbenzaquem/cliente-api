package com.springboot.banco.exemplo.clienteapi.services;

import com.springboot.banco.exemplo.clienteapi.entities.Cliente;
import com.springboot.banco.exemplo.clienteapi.repositories.ClienteRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class ClienteService {

    private final ClienteRepository clienteRepository;

    public Cliente save(Cliente cliente) {
        cliente.setId(null);
        return clienteRepository.save(cliente);
    }
}
