package com.springboot.banco.exemplo.clienteapi.repositories;

import com.springboot.banco.exemplo.clienteapi.entities.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepository extends JpaRepository<Cliente,Long> {
}
