package com.springboot.banco.exemplo.clienteapi.resources.exceptions;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class ResourceExceptionHandler {

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<StandardError> dataIntegrityViolationExceptionHandler(DataIntegrityViolationException ex, HttpServletRequest request) {
        StandardError error = new StandardError();
        error.setTimeStamp(System.currentTimeMillis());
        error.setStatus(HttpStatus.BAD_REQUEST.value());
        error.setMessage(ex.getMessage());
        if (ex.getMessage().contains("EMAIL") || ex.getMessage().contains("email")) {
            error.setMessage("Erro de validação!");
            error.addError("email", "Email repetido ou nulo!");
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<StandardError> validationException(MethodArgumentNotValidException ex, HttpServletRequest request) {
        StandardError error = new StandardError();
        error.setTimeStamp(System.currentTimeMillis());
        error.setStatus(HttpStatus.BAD_REQUEST.value());
        error.setMessage("Erro de validação!");
        ex.getBindingResult().getFieldErrors().forEach(f -> {
            error.addError(f.getField(), f.getDefaultMessage());
        });
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }
}
