package com.springboot.banco.exemplo.clienteapi.dtos;

import com.springboot.banco.exemplo.clienteapi.entities.Cliente;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ClienteDTO {
    private String nome;
    @NotBlank(message = "Campo 'email' não pode ser nulo ou vazio!")
    @Email(message = "Compo 'email' mal formado.")
    private String email;
    @NotBlank(message = "Campo 'cpf' não pode ser nulo ou vazio!")
    @CPF(message = "Campo 'cpf' mal formado.")
    private String cpf;
    private Date dataNascimento;

    public ClienteDTO(Cliente cliente) {
        this.nome = cliente.getNome();
        this.email = cliente.getNome();
        this.cpf = cliente.getCpf();
        this.dataNascimento = cliente.getDataNascimento();
    }

    public static Cliente parseCliente(ClienteDTO dto){
        return new Cliente(null,dto.nome,dto.email,dto.cpf,dto.dataNascimento);
    }
}
