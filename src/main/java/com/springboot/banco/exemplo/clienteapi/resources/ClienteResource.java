package com.springboot.banco.exemplo.clienteapi.resources;

import com.springboot.banco.exemplo.clienteapi.dtos.ClienteDTO;
import com.springboot.banco.exemplo.clienteapi.entities.Cliente;
import com.springboot.banco.exemplo.clienteapi.services.ClienteService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@AllArgsConstructor
@RestController
@RequestMapping(path = "/clientes")
public class ClienteResource {

    private ClienteService clienteService;

    @PostMapping
    public ResponseEntity<Void> insert(@RequestBody @Valid ClienteDTO dto) {
        Long id = clienteService.save(ClienteDTO.parseCliente(dto)).getId();
        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri()
                .path("/{id}").buildAndExpand(id).toUri();
        return ResponseEntity.created(uri).build();
    }
}
